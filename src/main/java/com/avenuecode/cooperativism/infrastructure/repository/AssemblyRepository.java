package com.avenuecode.cooperativism.infrastructure.repository;

import com.avenuecode.cooperativism.domain.enums.YesOrNo;
import com.avenuecode.cooperativism.domain.model.Assembly;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AssemblyRepository extends JpaRepository<Assembly, Integer> {

    @Query("select count(a) from Assembly a join a.votes v where a.id = ?1 and v.yesOrNo = ?2")
    long countByYesOrNo(Integer id, YesOrNo yn);
}
