package com.avenuecode.cooperativism.application.DTO;

import lombok.Data;

@Data
public class AssemblyDTO {

    private Integer id;

    private Integer agendaId;

    private Integer timeOpenInMinutes;
}
