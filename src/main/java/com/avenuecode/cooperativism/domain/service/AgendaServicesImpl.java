package com.avenuecode.cooperativism.domain.service;

import com.avenuecode.cooperativism.application.DTO.AgendaDTO;
import com.avenuecode.cooperativism.application.DTO.AssociateDTO;
import com.avenuecode.cooperativism.domain.model.Agenda;
import com.avenuecode.cooperativism.domain.model.Associate;
import com.avenuecode.cooperativism.exceptions.AgendaNotFoundException;
import com.avenuecode.cooperativism.infrastructure.repository.AgendaRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AgendaServicesImpl implements AgendaServices{

    final Logger LOGGER = LoggerFactory.getLogger(AgendaServicesImpl.class);

    @Autowired
    private AgendaRepository agendaRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public AgendaServicesImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AgendaDTO saveAgenda(AgendaDTO agenda){
        AgendaDTO savedAgenda = agendaToAgendaDTO(agendaRepository.save(agendaDTOToAgenda(agenda)));
        LOGGER.info("AgendaServicesImpl.saveAgenda : agenda {} was saved", savedAgenda);
        return savedAgenda;
    }

    private Agenda agendaDTOToAgenda(AgendaDTO dto){
        return modelMapper.map(dto, Agenda.class);
    }

    private AgendaDTO agendaToAgendaDTO(Agenda model){
        return modelMapper.map(model, AgendaDTO.class);
    }

    @Override
    public Agenda getAgendaModelById(Integer id) throws AgendaNotFoundException {
        Optional<Agenda> a = agendaRepository.findById(id);
        if(Optional.ofNullable(a).isPresent() && ! a.isEmpty()){
            return a.get();
        }
        throw new AgendaNotFoundException(String.format("Agenda id %s was not found", id));
    }
}
