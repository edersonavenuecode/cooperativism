package com.avenuecode.cooperativism.unitTest;

import com.avenuecode.cooperativism.application.DTO.AgendaDTO;
import com.avenuecode.cooperativism.domain.model.Agenda;
import com.avenuecode.cooperativism.domain.service.AgendaServices;
import com.avenuecode.cooperativism.exceptions.AgendaNotFoundException;
import com.avenuecode.cooperativism.infrastructure.repository.AgendaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AgendaServicesTest {

    public static AgendaDTO agenda1;
    public static AgendaDTO agendaFromDB;

    private final ModelMapper modelMapper;

    @Autowired
    private AgendaServices agendaServices;

    @MockBean
    private AgendaRepository agendaRepository;

    @Autowired
    public AgendaServicesTest(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @BeforeAll
    public static void loadContext(){
        agenda1 = new AgendaDTO();
        agenda1.setDescription("The first agenda");

        agendaFromDB = new AgendaDTO();
        agendaFromDB.setDescription("The first agenda");
        agendaFromDB.setId(99);
    }

    @Test
    public void shouldSaveAgenda(){
        when(agendaRepository.save(Mockito.any(Agenda.class))).thenReturn(agendaDTOToAgenda(agendaFromDB));

        AgendaDTO agendafromService = agendaServices.saveAgenda(agenda1);
        Assertions.assertEquals(agendafromService, agendaFromDB);

    }

    @Test
    public void shouldThrowAgendaNotFoundException(){

        when(agendaRepository.findById(5)).thenReturn(null);

        assertThrows(AgendaNotFoundException.class,
                () -> {
                    agendaServices.getAgendaModelById(5);
                }
        );

    }

    private Agenda agendaDTOToAgenda(AgendaDTO dto){
        return modelMapper.map(dto, Agenda.class);
    }

}
