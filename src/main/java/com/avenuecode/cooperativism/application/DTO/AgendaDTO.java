package com.avenuecode.cooperativism.application.DTO;

import lombok.Data;

@Data
public class AgendaDTO {

    private Integer id;

    private String description;

}
