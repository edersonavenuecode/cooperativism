package com.avenuecode.cooperativism.domain.enums;

public enum YesOrNo {
    NO,
    YES
}
