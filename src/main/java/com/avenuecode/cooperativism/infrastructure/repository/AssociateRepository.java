package com.avenuecode.cooperativism.infrastructure.repository;

import com.avenuecode.cooperativism.domain.model.Associate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssociateRepository extends JpaRepository<Associate, Integer> {
}
