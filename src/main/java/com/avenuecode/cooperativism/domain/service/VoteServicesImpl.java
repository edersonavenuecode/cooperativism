package com.avenuecode.cooperativism.domain.service;

import com.avenuecode.cooperativism.application.DTO.VoteDTO;
import com.avenuecode.cooperativism.domain.model.Assembly;
import com.avenuecode.cooperativism.domain.model.Vote;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssociateHasAlreadyVotedException;
import com.avenuecode.cooperativism.exceptions.AssociateNotFoundException;
import com.avenuecode.cooperativism.exceptions.TimeAssemblyExpiredException;
import com.avenuecode.cooperativism.infrastructure.repository.VoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class VoteServicesImpl implements VoteServices{

    final Logger LOGGER = LoggerFactory.getLogger(VoteServicesImpl.class);

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private AssociateServices associateServices;

    @Autowired
    private AssemblyServices assemblyServices;

    @Override
    public Vote saveVote(VoteDTO voteDTO) throws AssemblyNotFoundException, TimeAssemblyExpiredException, AssociateNotFoundException, AssociateHasAlreadyVotedException {
        if(voteRepository.countVoteByAssociateAndAssembly(voteDTO.getAssociateId(), voteDTO.getAssemblyId()) > 0){
            throw new AssociateHasAlreadyVotedException(String.format("Associate id %s has already voted on assembly id %s",
                    voteDTO.getAssociateId(), voteDTO.getAssemblyId()));
        }
        var assembly = assemblyServices.getAssemblyModelById(voteDTO.getAssemblyId());
        checkIfAssemblyIsNotExpired(assembly);
        var associate = associateServices.getAssociateModelById(voteDTO.getAssociateId());
        Vote vote = new Vote();
        vote.setAssembly(assembly);
        vote.setAssociate(associate);
        vote.setYesOrNo(voteDTO.getYesOrNo());

        Vote savedVote = voteRepository.save(vote);
        LOGGER.info("VoteServicesImpl.saveVote : vote {} was saved", savedVote);
        return savedVote;
    }

    private void checkIfAssemblyIsNotExpired(Assembly assembly) throws TimeAssemblyExpiredException {
        if(assembly.getFinishTime().before(new Date())){
            throw new TimeAssemblyExpiredException(String.format("Assembly id %s is expired", assembly.getId()));
        }
    }

    private VoteDTO voteToVoteDTO(Vote vote){
        VoteDTO dto = new VoteDTO();
        dto.setAssociateId(vote.getAssociate().getId());
        dto.setYesOrNo(vote.getYesOrNo());
        dto.setId(vote.getId());
        dto.setAssemblyId(vote.getAssembly().getId());
        return dto;
    }
}
