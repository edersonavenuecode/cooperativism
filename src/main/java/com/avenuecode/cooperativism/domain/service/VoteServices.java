package com.avenuecode.cooperativism.domain.service;

import com.avenuecode.cooperativism.application.DTO.VoteDTO;
import com.avenuecode.cooperativism.domain.model.Vote;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssociateHasAlreadyVotedException;
import com.avenuecode.cooperativism.exceptions.AssociateNotFoundException;
import com.avenuecode.cooperativism.exceptions.TimeAssemblyExpiredException;
import org.springframework.stereotype.Service;

@Service
public interface VoteServices {

    public Vote saveVote(VoteDTO vote) throws AssemblyNotFoundException, TimeAssemblyExpiredException, AssociateNotFoundException, AssociateHasAlreadyVotedException;
}
