package com.avenuecode.cooperativism.domain.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table
@Data
public class Associate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String cpf;
}
