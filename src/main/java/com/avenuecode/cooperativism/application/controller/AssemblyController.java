package com.avenuecode.cooperativism.application.controller;

import com.avenuecode.cooperativism.application.DTO.AssemblyDTO;
import com.avenuecode.cooperativism.application.DTO.ResultDTO;
import com.avenuecode.cooperativism.domain.model.Assembly;
import com.avenuecode.cooperativism.domain.service.AssemblyServices;
import com.avenuecode.cooperativism.exceptions.AgendaNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/assembly")
public class AssemblyController {

    final Logger LOGGER = LoggerFactory.getLogger(AssemblyController.class);

    @Autowired
    private AssemblyServices assemblyServices;

    @PostMapping
    public ResponseEntity<?> createNewAssembly(@RequestBody AssemblyDTO assembly) {
        try{
            return new ResponseEntity<Assembly>(assemblyServices.saveAssembly(assembly), HttpStatus.CREATED);
        } catch (AgendaNotFoundException e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }

    @GetMapping
    @RequestMapping(value = "/{id}")
    public ResponseEntity<?> getVotesByAssembly(@PathVariable Integer id){
        try{
            return new ResponseEntity<ResultDTO>(assemblyServices.getResultByAssembly(id), HttpStatus.CREATED);
        } catch (AssemblyNotFoundException e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }
}
