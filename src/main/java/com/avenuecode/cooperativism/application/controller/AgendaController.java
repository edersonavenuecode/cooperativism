package com.avenuecode.cooperativism.application.controller;

import com.avenuecode.cooperativism.application.DTO.AgendaDTO;
import com.avenuecode.cooperativism.domain.model.Agenda;
import com.avenuecode.cooperativism.domain.service.AgendaServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/agenda")
public class AgendaController {

    final Logger LOGGER = LoggerFactory.getLogger(AgendaController.class);

    @Autowired
    private AgendaServices agendaServices;

    @PostMapping
    public ResponseEntity<?> saveAgenda(@RequestBody AgendaDTO agenda){
        return new ResponseEntity<AgendaDTO>(agendaServices.saveAgenda(agenda), HttpStatus.CREATED);
    }
}
