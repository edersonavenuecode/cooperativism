package com.avenuecode.cooperativism.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
@Data
public class Assembly {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Agenda agenda;

    private Date finishTime;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "assembly")
    @JsonIgnore
    private List<Vote> votes = new ArrayList<Vote>();
}
