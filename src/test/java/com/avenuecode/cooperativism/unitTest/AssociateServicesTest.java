package com.avenuecode.cooperativism.unitTest;

import com.avenuecode.cooperativism.application.DTO.AssociateDTO;
import com.avenuecode.cooperativism.domain.model.Associate;
import com.avenuecode.cooperativism.domain.service.AssociateServices;
import com.avenuecode.cooperativism.infrastructure.repository.AssociateRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.Mockito.when;

@SpringBootTest
public class AssociateServicesTest {

    public static AssociateDTO associate1;
    public static AssociateDTO associateFromDB;

    private final ModelMapper modelMapper;

    @Autowired
    private AssociateServices associateServices;

    @MockBean
    private AssociateRepository repository;

    @Autowired
    public AssociateServicesTest(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @BeforeAll
    public static void loadContext(){
        associate1 = new AssociateDTO();
        associate1.setCpf("52256396070");
        associate1.setName("First Associate");

        associateFromDB = new AssociateDTO();
        associateFromDB.setCpf("52256396070");
        associateFromDB.setName("First Associate");
        associateFromDB.setId(12);

    }

    @Test
    public void shouldSaveAssociate(){
        when(repository.save(Mockito.any(Associate.class))).thenReturn(associateDTOToAssociate(associateFromDB));

        AssociateDTO associatefromService = associateServices.saveAssociate(associate1);
        Assertions.assertEquals(associatefromService, associateFromDB);

    }

    private Associate associateDTOToAssociate(AssociateDTO dto){
        return modelMapper.map(dto, Associate.class);
    }
}
