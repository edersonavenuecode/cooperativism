# Cooperativism



## Getting started

To run the application execute the commands bellow on the root directory
```
docker-compose up -d
./mvnw spring-boot:run
```
## Api URLs

- To create a new associate:

POST - http://localhost:8080/associate


```
{
    "name": "Fisrst Associate",
    "cpf" : "52256396070"
}
```

- To create a new agenda
POST - http://localhost:8080/agenda

Example of parameters
```
{
    "description": "Asembleia geral"
}
```
- To create a new assembly
  POST - http://localhost:8080/assembly

Example of parameters
```
{
    "agendaId": 1,
    "timeOpenInMinutes" : 5
}
```
- To vote on a assembly
  POST - http://localhost:8080/vote

Example of parameters (you can only use YES or NO as an answer of "yesOrNo")
```
{
    "associateId": 1,
    "assemblyId" : 1,
    "yesOrNo" : "NO"
}
```
- To check votes on a assembly
GET - http://localhost:8080/assembly/{id}


```
It's not necessary body, only replace the ID for the assembly ID 
```
