package com.avenuecode.cooperativism.domain.service;

import com.avenuecode.cooperativism.application.DTO.AssemblyDTO;
import com.avenuecode.cooperativism.application.DTO.ResultDTO;
import com.avenuecode.cooperativism.domain.model.Assembly;
import com.avenuecode.cooperativism.exceptions.AgendaNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import org.springframework.stereotype.Service;

@Service
public interface AssemblyServices {

    public Assembly saveAssembly(AssemblyDTO assembly) throws AgendaNotFoundException;

    public Assembly getAssemblyModelById(Integer id) throws AssemblyNotFoundException;

    public ResultDTO getResultByAssembly(Integer id) throws AssemblyNotFoundException;
}
