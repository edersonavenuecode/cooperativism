package com.avenuecode.cooperativism.exceptions;

public class AgendaNotFoundException extends Exception {
    public AgendaNotFoundException(String message) { super(message); }
}
