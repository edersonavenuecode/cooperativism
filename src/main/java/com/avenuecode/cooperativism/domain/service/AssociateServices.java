package com.avenuecode.cooperativism.domain.service;

import com.avenuecode.cooperativism.application.DTO.AssociateDTO;
import com.avenuecode.cooperativism.domain.model.Associate;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssociateNotFoundException;
import org.springframework.stereotype.Service;

@Service
public interface AssociateServices {

    public AssociateDTO saveAssociate(AssociateDTO associate);

    public Associate getAssociateModelById(Integer id) throws AssociateNotFoundException;

}
