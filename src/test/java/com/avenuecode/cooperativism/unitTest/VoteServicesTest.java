package com.avenuecode.cooperativism.unitTest;

import com.avenuecode.cooperativism.application.DTO.VoteDTO;
import com.avenuecode.cooperativism.domain.enums.YesOrNo;
import com.avenuecode.cooperativism.domain.model.Agenda;
import com.avenuecode.cooperativism.domain.model.Assembly;
import com.avenuecode.cooperativism.domain.model.Associate;
import com.avenuecode.cooperativism.domain.model.Vote;
import com.avenuecode.cooperativism.domain.service.AssemblyServices;
import com.avenuecode.cooperativism.domain.service.AssociateServices;
import com.avenuecode.cooperativism.domain.service.VoteServices;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssociateHasAlreadyVotedException;
import com.avenuecode.cooperativism.exceptions.AssociateNotFoundException;
import com.avenuecode.cooperativism.exceptions.TimeAssemblyExpiredException;
import com.avenuecode.cooperativism.infrastructure.repository.VoteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.mockito.Mockito.when;

@SpringBootTest
public class VoteServicesTest {

    public static VoteDTO vote1;
    public static Vote voteFromDB;
    public static Assembly assembly1;
    public static Associate associate1;
    public static Agenda agenda1;

    @Autowired
    private VoteServices voteServices;

    @MockBean
    private VoteRepository voteRepository;

    @MockBean
    private AssemblyServices assemblyServices;

    @MockBean
    private AssociateServices associateServices;

    @BeforeAll
    public static void loadContext(){
        vote1 = new VoteDTO();
        vote1.setAssemblyId(1);
        vote1.setAssociateId(1);

        agenda1 = new Agenda();
        agenda1.setId(1);
        agenda1.setDescription("The first agenda");

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(new java.util.Date());
        gc.add(Calendar.MINUTE,5);


        assembly1 = new Assembly();
        assembly1.setId(1);
        assembly1.setAgenda(agenda1);
        assembly1.setFinishTime(gc.getTime());

        associate1 = new Associate();
        associate1.setId(1);
        associate1.setCpf("52256396070");
        associate1.setName("First Associate");

        voteFromDB = new Vote();
        voteFromDB.setYesOrNo(YesOrNo.YES);
        voteFromDB.setAssociate(associate1);
        voteFromDB.setAssembly(assembly1);
    }

    @Test
    public void shouldSaveVote() throws AssociateNotFoundException, TimeAssemblyExpiredException, AssemblyNotFoundException, AssociateHasAlreadyVotedException {
        when(voteRepository.save(Mockito.any(Vote.class))).thenReturn(voteFromDB);
        when(associateServices.getAssociateModelById(1)).thenReturn(associate1);
        when(assemblyServices.getAssemblyModelById(1)).thenReturn(assembly1);

        Vote voteFromServices = voteServices.saveVote(vote1);
        Assertions.assertEquals(voteFromServices, voteFromDB);
    }


}
