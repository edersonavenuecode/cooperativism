package com.avenuecode.cooperativism.domain.service;

import com.avenuecode.cooperativism.application.DTO.AssemblyDTO;
import com.avenuecode.cooperativism.application.DTO.ResultDTO;
import com.avenuecode.cooperativism.domain.enums.YesOrNo;
import com.avenuecode.cooperativism.domain.model.Assembly;
import com.avenuecode.cooperativism.exceptions.AgendaNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import com.avenuecode.cooperativism.infrastructure.repository.AgendaRepository;
import com.avenuecode.cooperativism.infrastructure.repository.AssemblyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

@Service
public class AssemblyServicesImpl implements AssemblyServices{

    private static Integer MINUTES_DEFAULT = 1;

    final Logger LOGGER = LoggerFactory.getLogger(AssemblyServicesImpl.class);

    @Autowired
    private AssemblyRepository assemblyRepository;

    @Autowired
    private AgendaRepository agendaRepository;

    @Autowired
    private AgendaServices agendaServices;

    @Override
    public Assembly saveAssembly(AssemblyDTO assemblyDTO) throws AgendaNotFoundException {
        Assembly assembly =  new Assembly();
        assembly.setAgenda(agendaServices.getAgendaModelById(assemblyDTO.getAgendaId()));
        assembly.setFinishTime(dateWithPlus(assemblyDTO.getTimeOpenInMinutes()));
        Assembly savedAssembly = assemblyRepository.save(assembly);
        LOGGER.info("AssemblyServicesImpl.saveAssembly : assembly {} was saved", savedAssembly);
        return savedAssembly;
    }

    @Override
    public Assembly getAssemblyModelById(Integer id) throws AssemblyNotFoundException {
        Optional<Assembly> a = assemblyRepository.findById(id);
        if(Optional.ofNullable(a).isPresent() && ! a.isEmpty()){
            return a.get();
        }
        throw new AssemblyNotFoundException(String.format("Assembly id %s was not found", id));
    }

    @Override
    public ResultDTO getResultByAssembly(Integer id) throws AssemblyNotFoundException {
        Optional<Assembly> a = assemblyRepository.findById(id);
        if(Optional.ofNullable(a).isPresent() && ! a.isEmpty()){
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.setQuantityNO(assemblyRepository.countByYesOrNo(id, YesOrNo.NO));
            resultDTO.setQuantityYes(assemblyRepository.countByYesOrNo(id, YesOrNo.YES));

            return resultDTO;
        }
        throw new AssemblyNotFoundException(String.format("Assembly id %s was not found", id));

    }

    private Date dateWithPlus(Integer minutes){
        minutes = minutes==null ? MINUTES_DEFAULT : minutes;

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(new java.util.Date());
        gc.add(Calendar.MINUTE,minutes);
        return gc.getTime();
    }
}
