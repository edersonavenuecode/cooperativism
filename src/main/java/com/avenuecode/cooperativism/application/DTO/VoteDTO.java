package com.avenuecode.cooperativism.application.DTO;

import com.avenuecode.cooperativism.domain.enums.YesOrNo;
import lombok.Data;

@Data
public class VoteDTO {

    private Integer id;

    private Integer associateId;

    private Integer assemblyId;

    private YesOrNo yesOrNo;

}
