package com.avenuecode.cooperativism.exceptions;

public class AssemblyNotFoundException extends Exception {
    public AssemblyNotFoundException(String message) { super(message); }
}
