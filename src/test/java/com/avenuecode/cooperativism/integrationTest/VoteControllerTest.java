package com.avenuecode.cooperativism.integrationTest;

import com.avenuecode.cooperativism.CooperativismApplication;
import com.avenuecode.cooperativism.application.DTO.AssemblyDTO;
import com.avenuecode.cooperativism.domain.model.Agenda;
import com.avenuecode.cooperativism.domain.model.Associate;
import com.avenuecode.cooperativism.domain.service.AssemblyServices;
import com.avenuecode.cooperativism.exceptions.AgendaNotFoundException;
import com.avenuecode.cooperativism.infrastructure.repository.AgendaRepository;
import com.avenuecode.cooperativism.infrastructure.repository.AssemblyRepository;
import com.avenuecode.cooperativism.infrastructure.repository.AssociateRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@ContextConfiguration(classes={CooperativismApplication.class})
@AutoConfigureMockMvc(addFilters = false)
public class VoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AssemblyRepository assemblyRepository;

    @Autowired
    private AssociateRepository associateRepository;

    @Autowired
    private AgendaRepository agendaRepository;

    @Autowired
    private AssemblyServices assemblyServices;


    public void loadContext() throws AgendaNotFoundException {
        Associate associate = new Associate();
        associate.setCpf("52256396070");
        associate.setName("First Associate");
        associateRepository.save(associate);

        Agenda agenda = new Agenda();
        agenda.setDescription("The first agenda");
        agendaRepository.save(agenda);

        AssemblyDTO assembly = new AssemblyDTO();
        assembly.setAgendaId(1);
        assembly.setTimeOpenInMinutes(10);
        assemblyServices.saveAssembly(assembly);
    }

    @Test
    public void shouldVote() throws Exception {
        loadContext();
        mockMvc.perform(post("/vote")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"associateId\": \"1\",\"assemblyId\": \"1\", \"yesOrNo\": \"NO\", \"email\" : \"user@user.com\", \"phone\" : 199999  }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("1"));
    }
}
