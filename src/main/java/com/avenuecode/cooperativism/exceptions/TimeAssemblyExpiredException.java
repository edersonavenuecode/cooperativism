package com.avenuecode.cooperativism.exceptions;

public class TimeAssemblyExpiredException extends Exception {
    public TimeAssemblyExpiredException(String message) { super(message); }
}
