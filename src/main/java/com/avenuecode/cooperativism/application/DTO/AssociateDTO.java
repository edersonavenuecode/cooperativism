package com.avenuecode.cooperativism.application.DTO;

import lombok.Data;

@Data
public class AssociateDTO {

    private Integer id;

    private String name;

    private String cpf;
}
