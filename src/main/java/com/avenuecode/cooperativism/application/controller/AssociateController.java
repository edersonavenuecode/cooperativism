package com.avenuecode.cooperativism.application.controller;

import com.avenuecode.cooperativism.application.DTO.AssociateDTO;
import com.avenuecode.cooperativism.domain.service.AssociateServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( value = "/associate")
public class AssociateController {

    final Logger LOGGER = LoggerFactory.getLogger(AssociateController.class);

    @Autowired
    private AssociateServices associateServices;

    @PostMapping
    public ResponseEntity<?> createNewAssociate(@RequestBody AssociateDTO associate) {
        return new ResponseEntity<AssociateDTO>(associateServices.saveAssociate(associate), HttpStatus.CREATED);
    }
}
