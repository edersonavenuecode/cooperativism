package com.avenuecode.cooperativism.domain.service;

import com.avenuecode.cooperativism.application.DTO.AgendaDTO;
import com.avenuecode.cooperativism.domain.model.Agenda;
import com.avenuecode.cooperativism.exceptions.AgendaNotFoundException;
import org.springframework.stereotype.Service;

@Service
public interface AgendaServices {

    public AgendaDTO saveAgenda(AgendaDTO agenda);

    public Agenda getAgendaModelById(Integer id) throws AgendaNotFoundException;
}
