package com.avenuecode.cooperativism.domain.model;

import com.avenuecode.cooperativism.domain.enums.YesOrNo;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table
@Data
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Associate associate;

    @ToString.Exclude
    @ManyToOne
    private Assembly assembly;

    private YesOrNo yesOrNo;

}
