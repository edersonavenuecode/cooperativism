package com.avenuecode.cooperativism.application.controller;

import com.avenuecode.cooperativism.application.DTO.VoteDTO;
import com.avenuecode.cooperativism.domain.model.Vote;
import com.avenuecode.cooperativism.domain.service.VoteServices;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssociateHasAlreadyVotedException;
import com.avenuecode.cooperativism.exceptions.AssociateNotFoundException;
import com.avenuecode.cooperativism.exceptions.TimeAssemblyExpiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/vote")
public class VoteController {

    @Autowired
    private VoteServices voteServices;

    @PostMapping
    public ResponseEntity<?> saveVote(@RequestBody VoteDTO vote){
        try{
            return new ResponseEntity<Vote>(voteServices.saveVote(vote), HttpStatus.CREATED);
        } catch (AssociateNotFoundException e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        } catch (TimeAssemblyExpiredException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        } catch (AssemblyNotFoundException e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        } catch (AssociateHasAlreadyVotedException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }
}
