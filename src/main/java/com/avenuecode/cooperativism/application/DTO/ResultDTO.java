package com.avenuecode.cooperativism.application.DTO;

import lombok.Data;

@Data
public class ResultDTO {

    private long quantityYes;

    private long quantityNO;

}
