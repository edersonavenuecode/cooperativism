package com.avenuecode.cooperativism.exceptions;

public class AssociateNotFoundException extends Exception {
    public AssociateNotFoundException(String message) { super(message); }
}
