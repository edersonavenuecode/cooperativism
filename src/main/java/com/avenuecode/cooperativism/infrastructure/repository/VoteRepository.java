package com.avenuecode.cooperativism.infrastructure.repository;

import com.avenuecode.cooperativism.domain.enums.YesOrNo;
import com.avenuecode.cooperativism.domain.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Integer> {

    @Query("select count(v) from Vote v where v.associate.id = ?1 and v.assembly.id = ?2")
    long countVoteByAssociateAndAssembly(Integer associateId, Integer assemblyId);
}
