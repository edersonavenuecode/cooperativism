package com.avenuecode.cooperativism.exceptions;

public class AssociateHasAlreadyVotedException extends Exception {
    public AssociateHasAlreadyVotedException(String message) { super(message); }
}
