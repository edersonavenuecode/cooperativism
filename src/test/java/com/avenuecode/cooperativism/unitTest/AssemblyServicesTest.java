package com.avenuecode.cooperativism.unitTest;

import com.avenuecode.cooperativism.application.DTO.AssemblyDTO;
import com.avenuecode.cooperativism.domain.model.Agenda;
import com.avenuecode.cooperativism.domain.model.Assembly;
import com.avenuecode.cooperativism.domain.service.AgendaServices;
import com.avenuecode.cooperativism.domain.service.AssemblyServices;
import com.avenuecode.cooperativism.exceptions.AgendaNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import com.avenuecode.cooperativism.infrastructure.repository.AssemblyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AssemblyServicesTest {

    public static AssemblyDTO assembly1;
    public static Assembly assemblyFromDB;
    public static Agenda agenda1;

    @Autowired
    private AssemblyServices assemblyServices;

    @MockBean
    private AssemblyRepository assemblyRepository;

    @MockBean
    private AgendaServices agendaServices;


    @BeforeAll
    public static void loadContext(){
        assembly1 = new AssemblyDTO();
        assembly1.setAgendaId(1);
        assembly1.setTimeOpenInMinutes(10);

        agenda1 = new Agenda();
        agenda1.setId(1);
        agenda1.setDescription("The first agenda");


        assemblyFromDB = new Assembly();
        assemblyFromDB.setAgenda(agenda1);
        assemblyFromDB.setFinishTime(dateWithPlus(10));
    }

    @Test
    public void shouldSaveAssembly() throws AgendaNotFoundException {
        when(assemblyRepository.save(Mockito.any(Assembly.class))).thenReturn(assemblyFromDB);
        when(agendaServices.getAgendaModelById(1)).thenReturn(agenda1);

        Assembly assemblyromService = assemblyServices.saveAssembly(assembly1);
        Assertions.assertEquals(assemblyromService, assemblyFromDB);

    }

    @Test
    public void shouldThrowAssemblyNotFoundException(){
        when(assemblyRepository.findById(5)).thenReturn(null);

        assertThrows(AssemblyNotFoundException.class,
                () -> {
                    assemblyServices.getAssemblyModelById(5);
                }
        );
    }

    private static Date dateWithPlus(Integer minutes){

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(new java.util.Date());
        gc.add(Calendar.MINUTE,minutes);
        return gc.getTime();
    }

}
