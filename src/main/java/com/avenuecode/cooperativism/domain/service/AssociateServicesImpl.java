package com.avenuecode.cooperativism.domain.service;

import com.avenuecode.cooperativism.application.DTO.AssociateDTO;
import com.avenuecode.cooperativism.domain.model.Assembly;
import com.avenuecode.cooperativism.domain.model.Associate;
import com.avenuecode.cooperativism.exceptions.AssemblyNotFoundException;
import com.avenuecode.cooperativism.exceptions.AssociateNotFoundException;
import com.avenuecode.cooperativism.infrastructure.repository.AssociateRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AssociateServicesImpl implements AssociateServices{

    final Logger LOGGER = LoggerFactory.getLogger(AssociateServicesImpl.class);
    @Autowired
    public AssociateRepository associateRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public AssociateServicesImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AssociateDTO saveAssociate(AssociateDTO associate){
        AssociateDTO savedAssociate = associateToAssociateDTO(associateRepository.save(associateDTOToAssociate(associate)));
        LOGGER.info("AssociateServicesImpl.saveAssociate : associate {} was saved", savedAssociate);
        return savedAssociate;
    }

    @Override
    public Associate getAssociateModelById(Integer id) throws AssociateNotFoundException {
        Optional<Associate> a = associateRepository.findById(id);
        if(Optional.ofNullable(a).isPresent() && ! a.isEmpty()){
            return a.get();
        }
        throw new AssociateNotFoundException(String.format("Associate id %s was not found", id));
    }

    private Associate associateDTOToAssociate(AssociateDTO dto){
        return modelMapper.map(dto, Associate.class);
    }

    private AssociateDTO associateToAssociateDTO(Associate model){
        return modelMapper.map(model, AssociateDTO.class);
    }

}
